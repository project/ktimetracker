<?php
/**
 * Field handler to present a link punch delete.
 */
class views_handler_field_punch_link_delete extends views_handler_field_punch_link {
  function construct() {
    parent::construct();
    $this->additional_fields['pid'] = 'pid';
    $this->additional_fields['uid'] = 'uid';
  }

  function render($values) {
    // ensure user has access to delete this punch.
    $punch = new stdClass();
    $punch->pid = $values->{$this->aliases['pid']};
    $punch->uid = $values->{$this->aliases['uid']};
    if (!ktimetracker_punch_access('delete', $punch)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
    return l($text, "punch/$punch->pid/delete", array('query' => drupal_get_destination()));
  }
}

