<?php
/**
 * Field handler to provide simple renderer that allows using a themed user link
 */
class views_handler_field_punch_duration extends views_handler_field {
  /**
   * Add uid in the query so we can test for anonymous if needed.
   */
  function init(&$view, &$data) {
    parent::init($view, $data);
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['roundto'] = array('default' => '0');
    
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['raw'] = array(
      '#type' => 'checkbox',
      '#title' => t('Duration raw'),
      '#description' => t('If selected, the field will not be formatted.'),
      '#default_value' => !empty($this->options['raw']),
    );
    
    $form['roundto'] = array(
      '#type' => 'textfield',
      '#title' => t('Round to'),
      '#description' => t('If not zero, the duration will be rounded to the nearest multiple of the value of this field in seconds'),
      '#default_value' => $this->options['roundto'],
    );
    
  }
    
  function render($values) {
    $output = $values->{$this->field_alias};
    
    if ($this->options['raw']) {
      return $values->{$this->field_alias};
    }
    return ktimetracker_format_seconds($values->{$this->field_alias}, $this->options['roundto']);
  }
}

