<?php
/**
 * Field handler to present a link punch edit.
 */
class views_handler_field_punch_link_edit extends views_handler_field_punch_link {
  function construct() {
    parent::construct();
    $this->additional_fields['pid'] = 'pid';
    $this->additional_fields['uid'] = 'uid';
  }

  function render($values) {
    // ensure user has access to edit this punch.
    $punch = new stdClass();
    $punch->pid = $values->{$this->aliases['pid']};
    $punch->uid = $values->{$this->aliases['uid']};
    if (!ktimetracker_punch_access('update', $punch)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    return l($text, "punch/$punch->pid/edit", array('query' => drupal_get_destination()));
  }
}

