<?php
/**
* Implementation of views_plugin_style
*/

class ktimetracker_plugin_style extends views_plugin_style_table {
  function init(&$view, &$display) {
    $this->view = &$view;
    $this->display = &$display;

    // Overlay incoming options on top of defaults
    $this->unpack_options($this->options, isset($options) ? $options : $display->handler->get_option('style_options'));

    if ($this->uses_row_plugin() && $display->handler->get_option('row_plugin')) {
      $this->row_plugin = $display->handler->get_plugin('row');
    }
    $this->num_groups = 5;
    
    for ($i=1; $i < $this->num_groups; $i++) { 
      $this->options += array(
        'grouping_'. $i => '',
        );
    }
  
    
    $this->definition += array(
      'uses grouping' => TRUE,
    );
  }
  
  function option_definition() {
    $options = parent::option_definition();
    for ($i=1; $i < $this->num_groups; $i++) { 
      $options['grouping_'].$i = array('default' => '');
    }
    
    $options['total'] = array('default' => '');
    $options['roundto'] = array('default' => '0');
    
    return $options;
  }

  /* Options Form */

 function options_form(&$form, &$form_state) {
   $options = parent::options_form(&$form, &$form_state);
   
   // Only fields-based views can handle grouping.  Style plugins can also exclude
   // themselves from being groupable by setting their "use grouping" definiton
   // key to FALSE.
   // @TODO: Document "uses grouping" in docs.php when docs.php is written.
   if ($this->uses_fields() && $this->definition['uses grouping']) {
     $options = array('' => t('<None>'));
     foreach ($this->display->handler->get_handlers('field') as $field => $handler) {

       if ($label = $handler->label()) {
         $options[$field] = $label;
       }
       else {
         $options[$field] = $handler->ui_name();
       }
     }
     
     // Grouping options
     
     for ($i=1; $i < $this->num_groups; $i++) { 
       // If there are no fields, we can't group on them.
       if (count($options) > 1) {
         $form['grouping_'.$i] = array(
           '#type' => 'select',
           '#title' => t('Grouping field @num', array('@num' => $i)),
           '#options' => $options,
           '#default_value' => $this->options['grouping_'. $i],
           '#description' => t('You may optionally specify a field by which to group the records. Leave blank to not group.'),
         );
       }
     }
     
     $form['total'] = array(
       '#type' => 'select',
       '#title' => t('Total field'),
       '#options' => $options,
       '#default_value' => $this->options['total'],
       '#description' => t('You may optionally specify a field for which a total will be displayed for each group.'),
      );
      
      $form['roundto'] = array(
        '#type' => 'textfield',
        '#title' => t('Round total to'),
        '#description' => t('If not zero, the total will be rounded to the nearest multiple of the value of this field in seconds'),
        '#default_value' => $this->options['roundto'],
      );
   }
 }
 /**
  * Render the display in this style.
  */
 function render() {
   if ($this->uses_row_plugin() && empty($this->row_plugin)) {
     vpr('views_plugin_style_default: Missing row plugin');
     return;
   }

   // Group the rows according to the grouping field, if specified.
   
   $grouping = array();
   for ($i=1; $i < $this->num_groups; $i++) {
     if (!empty($this->options['grouping_'. $i])) {
       $grouping[] = $this->options['grouping_'. $i];
     }
   }

   $sets = $this->render_grouping($this->view->result, $grouping);

   // Render each group separately and concatenate.  Plugins may override this
   // method if they wish some other way of handling grouping.
   $output = '';
   foreach ($sets as $title => $records) {
     if ($this->uses_row_plugin()) {
       $rows = array();
       foreach ($records as $label => $row) {
         $rows[] = $this->row_plugin->render($row);
       }
     }
     else {
       $rows = $records;
     }

     $output .= theme($this->theme_functions(), $this->view, $this->options, $rows, $title);
   }

   return $output;
 }

 /**
  * Group records as needed for rendering.
  *
  * @param $records
  *   An array of records from the view to group.
  * @param $grouping_field
  *   The field id on which to group.  If empty, the result set will be given
  *   a single group with an empty string as a label.
  * @return
  *   The grouped record set.
  */
  function render_grouping($records, $grouping_field = '') {
    $sets = array();
    $field = array_shift($grouping_field);
    $total_field = $this->view->field[$this->options['total']]->field_alias;
    $rows = array();
    $total = 0;
    if (!$field) {
      $sets =  array (
        '' => array(
            'type' => 'rows',
            'rows' => $records
          )
      );
      
      // Calculate total
      
      if ($total_field) {
        foreach($records as $row) {
          $total += $row->{$total_field};
        }
        $sets['']['total'] = $total;
      }
    }
    else {
      $groups = array();

      foreach ($records as $row) {
        $grouping = '';
        // Group on the rendered version of the field, not the raw.  That way,
        // we can control any special formatting of the grouping field through
        // the admin or theme layer or anywhere else we'd like.
        if (isset($this->view->field[$field])) {
          $grouping = $this->view->field[$field]->theme($row);
          // if ($this->view->field[$field]->options['label']) {
          //   $grouping = $this->view->field[$field]->options['label'] . ': ' . $grouping;
          // }
        }
        
        $groups[$grouping][] = $row;
      }
      
      foreach($groups as $grouping => $group) {
        $total = 0;
        $rendered_groups = $this->render_grouping($group, $grouping_field);
        foreach ($rendered_groups as $rendered_group) {
          $total += $rendered_group['total'];
        }
        $sets[$grouping] = array(
            'type' => 'group',
            'total' => $total,
            'rows' => $rendered_groups
          );
      }
    }
    return $sets;
  }
}