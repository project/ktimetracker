<?php
/**
 * Field handler to provide simple renderer that allows using a computed end date
 */
class views_handler_field_punch_end extends views_handler_field_date {
  /**
   * Add uid in the query so we can test for anonymous if needed.
   */
  function init(&$view, &$data) {
    parent::init($view, $data);
    $this->additional_fields['begin'] = array('field' => 'begin');
    $this->additional_fields['duration'] = array('field' => 'duration');
  }
  
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
  
  function render($values) {
    if ($values->{$this->aliases['duration']} == -1) {
      $values->{$this->field_alias} = time() - $this->aliases['begin'];
    }
    else {
      $values->{$this->field_alias} = $values->{$this->aliases['begin']} + $values->{$this->aliases['duration']};
    }
    return parent::render($values);
  }
}

