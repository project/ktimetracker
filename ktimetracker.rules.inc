<?php


/**
* Implementation of hook_rules_data_type_info().
*/
function ktimetracker_rules_data_type_info() {
  return array(
    'punch' => array(
      'label' => t('Punch'),
      'class' => 'rules_data_type_punch',
      'savable' => FALSE,
      'identifiable' => TRUE,
      'uses_input_form' => FALSE,
      'module' => 'Punch',
    ),
  );
}

/**
* Defines the rules node data type.
*/
class ktimetracker_data_type_punch extends rules_data_type {
//   function save() {
//     $punch = &$this->get();
//     node_save($node);
//     return TRUE;
//   }

  function load($pid) {
    return punch_load($pid);
  }

  function get_identifier() {
    $punch = &$this->get();
    return $punch->pid;
  }
}