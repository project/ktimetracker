<?php
function ktimetracker_views_data() {
  $data = array(
    'kpunch' => array(
      'table' => array(
        'group' => t('Timetracker'),
        'base' => array(
          'field' => 'pid',
          'title' => t('Punch')
        ),
      ),
      'begin' => array(
        'title' => t('Begin'),
        'help' => t('Beginning of the punch'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
          'numeric' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
      ),
      'end' => array(
        'title' => t('End'),
        'help' => t('End of the punch'),
        'field' => array(
          'handler' => 'views_handler_field_punch_end',
          'click sortable' => TRUE,
        ),
        /*'filter' => array(
          'handler' => 'views_handler_filter_punch_end',
          'numeric' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),*/
      ),
      'billable_intern' => array(
        'title' => t('Billable internally'),
        'help' => t('Billable to internally'),
        'field' => array(
          'handler' => 'views_handler_field_boolean',
          'click sortable' => TRUE,
        )
      ),
      'billable_client' => array(
        'title' => t('Billable to the customer'),
        'help' => t('Billable to the customer'),
        'field' => array(
          'handler' => 'views_handler_field_boolean',
          'click sortable' => TRUE,
        )
      ),
      'rate' => array(
          'title' => t('Rate'),
          'help' => t('Rate'),
          'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE,
          )
      ),
      'duration' => array(
        'title' => t('Duration'),
        'help' => t('Duration of the punch'),
        'field' => array(
          'handler' => 'views_handler_field_punch_duration',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
          'numeric' => TRUE,
        ),
      ),
      'comment' => array(
        'field' => array(
          'handler' => 'views_handler_field',
          'title' => t('Comment'),
          'help' => t('Comment associated to the punch'),
        )
      ),
      'nid' => array(
        'field' => array(
          'handler' => 'views_handler_field_node',
          'title' => t('Node'),
          'help' => t('Node associated to the punch'),
        )
      ),
      'edit' => array(
        'field' => array(
          'title' => t('Edit link'),
          'help' => t('Provide a simple link to edit a punch.'),
          'handler' => 'views_handler_field_punch_link_edit',
        )
      ),
      'delete' => array(
        'field' => array(
          'title' => t('Delete link'),
          'help' => t('Provide a simple link to delete a punch.'),
          'handler' => 'views_handler_field_punch_link_delete',
        )
      ),

    ),

    "users" => array(
       'table' => array(
         'join' => array(
           'kpunch' => array(
             'left_field' => 'uid',
             'field' => 'uid',
             'type' => 'INNER'
           )
         )
       )
     ),

    "node" => array(
       'table' => array(
         'join' => array(
           'kpunch' => array(
             'left_field' => 'nid',
             'field' => 'nid',
           )
         )
       )
     )

  );
  
  $data['kpunch']['nid'] = array(
    'title' => t('Punched node'),
    'help' => t('Node that has been punched'),
    'relationship' => array(
      'title' => t('Punched node'),
      'help' => t("Node that has been punched."),
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node'),
    ),
  );

  $data['kpunch']['uid'] = array(
    'title' => t('Punched user'),
    'help' => t('User who has punched'),
    'relationship' => array(
      'title' => t('Punched user'),
      'help' => t("User who has punched."),
      'base' => 'user',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User'),
    ),
  );
    
  return $data;
}

function ktimetracker_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'ktimetracker') . '/views',
    ),
    'handlers' => array(
      'views_handler_field_punch_duration' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_punch_end' => array(
        'parent' => 'views_handler_field_date',
      ),
      'views_handler_field_punch_link' => array(
      'parent' => 'views_handler_field',
      ),
      'views_handler_field_punch_link_edit' => array(
        'parent' => 'views_handler_field_punch_link',
      ),
      'views_handler_field_punch_link_delete' => array(
        'parent' => 'views_handler_field_punch_link',
      ),

    ),
  );
}

/**
* Implementation of hook_views_plugins
*
*
*/

function ktimetracker_views_plugins() {
  $path = drupal_get_path('module', 'ktimetracker');
  $views_path = drupal_get_path('module', 'views');
  // require_once "./$path/theme/theme.inc";
  return array(
    'module' => 'ktimetracker', // This just tells our themes are elsewhere.
    'style' => array(
      'parent' => array(
        // this isn't really a display but is necessary so the file can
        // be included.
        'no ui' => TRUE,
        'handler' => 'views_plugin_style',
        'path' => "$views_path/plugins",
        'parent' => '',
      ),
      'table' => array(
        'no ui' => TRUE,
        'handler' => 'views_plugin_style_table',
        'path' => "$views_path/plugins",
        'parent' => 'parent',
        ),
      'ktimetracker_report' => array(
        'title' => t('Ktimetracker Report'),
        'help' => t('Display ktimetracker report.'),
        'handler' => 'ktimetracker_plugin_style',
        'path' => "$path/views",
        'parent' => 'table',
        'theme' => 'ktimetracker_report',
        'theme file' => 'theme.inc',
        'theme path' => "$path/theme",
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal', 
        'even empty' => TRUE,
      ),
    ),
  );
}

?>