<?php
function theme_ktimetracker_report($view, $options, $rows, $title) {
  drupal_add_js('misc/collapse.js');
  
    if ($rows['type'] == 'group') {
      $output = '<fieldset class="collapsible collapsed">';
      if (!empty($title)) {
        $output .= '<legend>'. $title  .' '. t('Total : @total', array('@total' => ktimetracker_format_seconds($rows['total'], $options['roundto']))) . '</legend>';
      }
      foreach ($rows['rows'] as $key => $value) {
        $output .= theme('ktimetracker_report', $view, $options, $value, $key);
      }
      $output .= '</fieldset>';
    }
    else {
      $output .= theme('views_view_table', $view, $options, $rows['rows'], $key);
      $output .= '<p>'. t('Total : @total', array('@total' => ktimetracker_format_seconds($rows['total'], $options['roundto']))).'</p>';
    }
  
  return $output;
}